import uvicorn
from fastapi import FastAPI, Depends
from sqlalchemy.orm import Session
import crud
import models
import schemas
from database import SessionLocal, engine


models.Base.metadata.create_all(bind=engine)


app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/")
async def root():
    return {"message": "hello world"}


@app.post("/books/", response_model=schemas.Book)
def create_book(book: schemas.Book, db: Session = Depends(get_db)):
    out = crud.create_book(db=db, book=book)
    print(f"{out=} {type(out)}")
    return out


@app.get("/books/")
def read_books(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    books = crud.get_books(db, skip=skip, limit=limit)
    return books


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8005)
