from pydantic import BaseModel


class BookBase(BaseModel):
    title: str


class Book(BookBase):
    id: int
    # owner_id: int

    class Config:
        orm_mode = True
